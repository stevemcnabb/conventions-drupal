<?php

declare(strict_types=1);

namespace ClientNameHere\ConventionsDrupal;

use GrumPHP\Runner\TaskResult;
use GrumPHP\Runner\TaskResultInterface;
use GrumPHP\Task\TaskInterface;
use GrumPHP\Task\Config\EmptyTaskConfig;
use GrumPHP\Task\Config\TaskConfigInterface;
use GrumPHP\Task\Context\ContextInterface;
use GrumPHP\Task\Context\GitPreCommitContext;
use GrumPHP\Task\Context\RunContext;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

/**
 * A custom grumphp task to inspect README.md files.
 */
class ReadmeInspector implements TaskInterface {

  /**
   * The task config interface.
   *
   * @var \GrumPHP\Task\Config\TaskConfigInterface
   */
  private $config;

  /**
   * The filesystem component.
   *
   * @var Symfony\Component\Filesystem\Filesystem
   */
  private $fileSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystem $fileSystem) {
    $this->config = new EmptyTaskConfig();
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function getConfigurableOptions(): OptionsResolver {
    $resolver = new OptionsResolver();
    $resolver->setDefaults([
      'allow_empty_readme' => FALSE,
      'warn_on_missing_readme' => FALSE,

    ]);
    return $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(): TaskConfigInterface {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function withConfig(TaskConfigInterface $config): TaskInterface {
    $new = clone $this;
    $new->config = $config;
    return $new;
  }

  /**
   * {@inheritdoc}
   */
  public function canRunInContext(ContextInterface $context): bool {
    return $context instanceof RunContext || $context instanceof GitPreCommitContext;
  }

  /**
   * {@inheritdoc}
   */
  public function run(ContextInterface $context): TaskResultInterface {

    $errorMessage = '';
    $config = $this->getConfig()->getOptions();

    // Check for a README.md file.
    if ($this->fileSystem->exists('README.md')) {
      $readme = new File('./README.md');

      if ($readme->getSize() > 1) {
        return TaskResult::createPassed($this, $context);
      }
      else {
        if (!$config['allow_empty_readme']) {
          $errorMessage .= "WARNING: Your readme file seems empty. Consider writing some documentation." . PHP_EOL;
        }
      }
    }
    else {
      if ($config['warn_on_missing_readme']) {
        $errorMessage .= 'WARNING: you are missing a README.md file.  Consider adding one to help others to understand your code.' . PHP_EOL;
      }
    }

    return TaskResult::createNonBlockingFailed($this, $context, $errorMessage);
  }

}

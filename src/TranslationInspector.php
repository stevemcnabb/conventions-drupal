<?php

declare(strict_types=1);

namespace ClientNameHere\ConventionsDrupal;

use Gettext\Loader\PoLoader;
use GrumPHP\Runner\TaskResult;
use GrumPHP\Runner\TaskResultInterface;
use GrumPHP\Task\TaskInterface;
use GrumPHP\Task\Config\EmptyTaskConfig;
use GrumPHP\Task\Config\TaskConfigInterface;
use GrumPHP\Task\Context\ContextInterface;
use GrumPHP\Task\Context\GitPreCommitContext;
use GrumPHP\Task\Context\RunContext;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * A custom grumphp task to check for translation coverage.
 */
class TranslationInspector implements TaskInterface {
  /**
   * The task config interface.
   *
   * @var \GrumPHP\Task\Config\TaskConfigInterface
   */
  private $config;

  /**
   * Dir where we are loading.
   *
   * @var string
   */
  private $dir;

  /**
   * {@inheritdoc}
   */
  public function __construct() {

    $this->dir = __DIR__;
    $this->config = new EmptyTaskConfig();
  }

  /**
   * {@inheritdoc}
   */
  public static function getConfigurableOptions(): OptionsResolver {

    $resolver = new OptionsResolver();
    $resolver->setDefaults([
      'language_ids' => ['fr'],
      'warn_on_missing_translation_strings' => TRUE,
      'warn_on_missing_languages' => TRUE,
    ]);

    $resolver->addAllowedTypes('language_ids', ['array']);

    return $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(): TaskConfigInterface {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function withConfig(TaskConfigInterface $config): TaskInterface {

    $new = clone $this;
    $new->config = $config;
    return $new;
  }

  /**
   * {@inheritdoc}
   */
  public function canRunInContext(ContextInterface $context): bool {

    return $context instanceof RunContext || $context instanceof GitPreCommitContext;
  }

  /**
   * {@inheritdoc}
   */
  public function run(ContextInterface $context): TaskResultInterface {

    $config = $this->getConfig()->getOptions();
    $errorMessage = '';
    $isDrupal = FALSE;

    $codeFileExtensions = [
      'inc',
      'php',
      'install',
      'profile',
      'theme',
      'module',
    ];
    $poFiles = [];
    $langsFound = [];
    $langsMissing = [];

    if (0 === $context->getFiles()->count()) {
      return TaskResult::createSkipped($this, $context);
    }

    $files = $context->getFiles()->ignoreSymlinks();

    // Is this a Drupal module, theme or profile?
    foreach ($files as $file) {

      if (stristr($file->getFileName(), '.info.yml')) {
        $isDrupal = TRUE;
      }
    }

    // This test only applies to Drupal modules, themes and profiles.
    if (!$isDrupal) {
      return TaskResult::createSkipped($this, $context);
    }

    // Find translations.
    foreach ($files as $file) {

      if (substr($file->getFileName(), strlen($file->getFileName()) - 3, 3) == '.po') {

        array_push($poFiles, $file);
        $fileNameParts = explode('.', $file->getFileName());
        array_push($langsFound, $fileNameParts[0]);
      }
    }

    $langsMissing = array_diff($config['language_ids'], $langsFound);

    // Are you missing any required languge translation .po files?
    if (count($langsMissing) > 0 && $config['warn_on_missing_languages']) {
      $errorMessage .= 'WARNING: No translation po file(s) exist for required langauge(s): ' . implode(',', $langsMissing) . PHP_EOL;
      $errorMessage .= "\nYou can greate them with the following command: \n\n mkdir ./translations && touch ";
      foreach ($langsMissing as $langId) {
        $errorMessage .= './translations/' . $langId . '.po ';
      }

      $errorMessage .= PHP_EOL;

      return TaskResult::createNonBlockingFailed($this, $context, $errorMessage);
    }

    // For all files that are code or twig, dispatch a parser.
    foreach ($files as $file) {

      $fileNameParts = explode('.', $file->getFileName());
      $fileExtension = $fileNameParts[1];

      if (in_array($fileExtension, $codeFileExtensions) || stristr($file->getFileName(), '.html.twig')) {

        $tStrings = [];

        if (stristr($file->getFileName(), 'html.twig')) {
          $tStrings = $this->getStringsFromTwigFile($file);
        }
        else {
          $tStrings = $this->getStringsFromCodeFile($file);
        }

        foreach ($poFiles as $poFile) {

          $poFileContents = $poFile->getContents();
          $poloader = new PoLoader();
          $translator = $poloader->loadString($poFileContents);
          $translations = $translator->getTranslations();
          $stringlist = [];
          $poFileMissing = [];

          foreach ($translations as $t) {
            $stringlist[] = $t->getOriginal();
          }

          foreach ($tStrings as $tString) {

            if (!in_array($tString, $stringlist) && $config['warn_on_missing_translation_strings']) {
              $errorMessage .= "String from code or twig >>>$tString<<< not found in " . $poFile->getFileName() . "\n";
            }
          }

          if (count($poFileMissing)) {
            $errorMessage .= implode(PHP_EOL, array_unique($poFileMissing));
          }
        }
      }
    }

    if ($errorMessage) {
      return TaskResult::createNonBlockingFailed($this, $context, $errorMessage);
    }
    return TaskResult::createPassed($this, $context);
  }

  /**
   * Get the t strings from a code file.
   */
  public function getStringsFromCodeFile($file) {

    $fileContents = $file->getContents();

    // Parse out t() and $this->t() function calls.
    $found = [];
    $matches = [];
    $matches_alt = [];
    $oopmatches = [];

    preg_match_all("/\s?[^a-z^A-Z ]+?t[(](.*)[)][;,]/", $fileContents, $matches);
    preg_match_all("/->t[(](.*)[)][;]/", $fileContents, $oopmatches);
    preg_match_all("/\st[(](.*)[)][;,]/", $fileContents, $matches_alt);

    $found = array_merge($matches[1], $oopmatches[1], $matches_alt[1]);

    return $this->normalizeStrings($found);
  }

  /**
   * Get the t strings from a twig template.
   */
  public function getStringsFromTwigFile($file) {

    $fileContents = $file->getContents();
    $matches = [];
    preg_match_all("/{{\s?+['\"]([^|]*)['\"]\s?+\|\s?+t\s?+}}/", $fileContents, $matches);
    return array_unique($matches[1]);
  }

  /**
   * Normalize the strings.
   */
  public function normalizeStrings($in) {

    $out = [];
    foreach ($in as $string) {

      // Ignore any variable translations. (yes, those violate phpcs).
      if (substr($string, 0, 1) == '$' || substr($string, 0, 3) == '\Dr') {
        continue;
      }

      // How is it quoted?
      $quote = substr($string, 0, 1);

      // We will use double-quotes for our patterns so must escape.
      if ($quote == '"') {
        $quote = '\"';
      }

      // Does this string look like a call to t that includes placeholders?
      if (preg_match("/\]\)?$/", $string)) {

        $pattern = "/^\s?+$quote|$quote,.*$/";
        $string = preg_replace($pattern, '', $string);
        $out[] = $string;
      }
      // This string looks like a '?  :' if statment shortform.
      elseif (stristr($string, '?') && stristr($string, ':')) {

        // Just grab the first quoted string, but allow for escaped quotes.
        $matches = [];
        preg_match("/^['\"]([^\\][^'\"]*)['\"]/", $string, $matches);

        if (!is_null($matches[1])) {
          $out[] = $matches[1];
        }
      }

      // This string was used as an argument for another function with inline t.
      elseif (stristr($string, "')") || stristr($string, '")')) {

        $index = strpos($string, '")');
        if (!$index) {
          $index = strpos($string, "')");
        }

        $normalized = substr($string, 0, $index);
        array_push($out, $normalized);
      }

      // Nope - looks like a regular string.
      else {

        // Snip off quotes and trailing parentheses.
        $string = preg_replace("/^$quote|$quote\)?$/", '', $string);
        $out[] = $string;
      }
    }

    // Snip off quotes.
    foreach ($out as $key => $outString) {
      if (substr($outString, 0, 1) == "'" || substr($outString, 0, 1) == "'") {
        $outString = substr($outString, 1, strlen($outString) - 1);
        $out[$key] = $outString;
      }
    }

    return array_unique($out);
  }

}

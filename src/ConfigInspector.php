<?php

declare(strict_types=1);

namespace ClientNameHere\ConventionsDrupal;

use GrumPHP\Runner\TaskResult;
use GrumPHP\Runner\TaskResultInterface;
use GrumPHP\Task\TaskInterface;
use GrumPHP\Task\Config\EmptyTaskConfig;
use GrumPHP\Task\Config\TaskConfigInterface;
use GrumPHP\Task\Context\ContextInterface;
use GrumPHP\Task\Context\GitPreCommitContext;
use GrumPHP\Task\Context\RunContext;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * A custom grumphp task to inspect config dirs for disallowed files.
 *
 * Customizable Parameters:
 *   - allow_uuids: if set true, allow uuids in files on first line.
 *   - allow_base_config_overrides: if true, allow core base field overrides
 *     in config yamls that are not part of a custom node type.
 */
class ConfigInspector implements TaskInterface {
  /**
   * The task config interface.
   *
   * @var \GrumPHP\Task\Config\TaskConfigInterface
   */
  private $config;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->config = new EmptyTaskConfig();
  }

  /**
   * {@inheritdoc}
   */
  public static function getConfigurableOptions(): OptionsResolver {
    $resolver = new OptionsResolver();
    $resolver->setDefaults([
      'ignore_whitelist' => [],
      'allow_uuids' => FALSE,
      'allow_base_config_overrides' => FALSE,
      'allow_non_yaml_files_in_configs' => FALSE,
    ]);

    $resolver->addAllowedTypes('ignore_whitelist', ['array']);

    return $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(): TaskConfigInterface {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function withConfig(TaskConfigInterface $config): TaskInterface {
    $new = clone $this;
    $new->config = $config;

    return $new;
  }

  /**
   * {@inheritdoc}
   */
  public function canRunInContext(ContextInterface $context): bool {
    return $context instanceof RunContext || $context instanceof GitPreCommitContext;
  }

  /**
   * {@inheritdoc}
   */
  public function run(ContextInterface $context): TaskResultInterface {

    $config = $this->getConfig()->getOptions();
    $errorMessage = '';
    $isDrupalModule = FALSE;

    if (0 === $context->getFiles()->count()) {
      return TaskResult::createSkipped($this, $context);
    }

    $files = $context->getFiles()
      ->ignoreSymlinks()
      ->notPaths($config['ignore_whitelist']);

    if ($files->count() > 0) {

      $customNodeTypes = [];

      // Iterate over files to find any custom node types.
      foreach ($files as $file) {

        // Is this a Drupal module, theme or profile?
        if (stristr($file->getFileName(), '.info.yml')) {
          $isDrupalModule = TRUE;
        }

        // Ignore non-config files.
        if (!stristr($file->getPath(), 'config/')) {
          continue;
        }

        // No hook_* directories permitted in config.
        if (stristr($file->getPath(), 'hook_')) {

          $errorMessage = 'Disallowed directory ' . $file->getPath() . ' found.  Please remove and run tests again.';
          return TaskResult::createFailed($this, $context, $errorMessage);
        }

        if (stristr($file->getFileName(), 'node.type.')) {

          $matches = [];
          preg_match("/^node\.type\.(.*)\.yml/", $file->getFileName(), $matches);
          $customNodeTypes[] = $matches[1];
        }
      }

      // Iterate over files looking for base overrides outside custom types.
      foreach ($files as $file) {

        // Ignore non-config files.
        if (!stristr($file->getPath(), '/config/')) {
          continue;
        }

        if (stristr($file->getFileName(), 'core.base_field_override.')) {

          // Is this created on one of our custom node types?
          $permitted = FALSE;

          // Are we using a config override to allow?
          if ($config['allow_base_config_overrides'] == TRUE) {
            $permitted = TRUE;
          }

          foreach ($customNodeTypes as $nodeType) {

            if (stristr($file->getFileName(), 'node.' . $nodeType)) {
              $permitted = TRUE;
            }
          }
          if (!$permitted) {
            $errorMessage .= "WARNING: core.base_field_override configuration '" . $file->getPath() . '/' . $file->getFileName() . "' found but is not part of a custom node type defined in this repository.  This approach is discouraged. Consider removing these." . PHP_EOL;
          }
        }

        // Does this config file contain uuids in the first line? If so, warn.
        $data = $file->getContents();
        $lines = explode("\n", $data);

        if (stristr($lines[0], 'uuid:') && $config['allow_uuids'] != TRUE) {
          $errorMessage .= "WARNING: file " . $file->getPath() . '/' . $file->getFileName()
            . ' contains a uuid on its first line.  If this is intentional, ignore this warning.'
            . ' Otherwise, please sanitize your config files!' . PHP_EOL;
        }

        // Drupal module, theme or profile? no non-yaml alowed in config/*.
        $extension = $file->getExtension();

        // Params passed to "notPaths" above won't trigger this error.
        if ($isDrupalModule && $extension != 'yml' && $extension != 'yaml' && $config['allow_non_yaml_files_in_configs'] != TRUE) {
          $errorMessage .= 'WARNING: invalid file found in config directory: ' . $file->getPath() . '/' . $file->getFileName()
            . " has non-yaml extension " . $extension . PHP_EOL;
        }
      }
      if ($errorMessage != '') {
        return TaskResult::createNonBlockingFailed($this, $context, $errorMessage);
      }
    }

    return TaskResult::createPassed($this, $context);
  }

}
